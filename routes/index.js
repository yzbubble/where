var express = require('express');
var router = express.Router();
var sites = require("../sites");

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {data:sites});
});

module.exports = router;
